const faverage = numbers => {
  let sum = 0;
  if (numbers == 0 || numbers == null) {
    return 0;
  } else {
    for (let i = 0; i < numbers.length; i++)
    {
      sum += numbers[i];
    }
    return sum/numbers.length;
  }

};

module.exports = faverage;
