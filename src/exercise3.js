/*
 * Returns a function that approximates the derive of fn with error h in the value given to the returned function.
 */
const fderive = (fn, h) => x => (fn(x+h)-fn(x-h))/(2.0*h);


const squear = x => Math.pow(x, 2);

//f(x+h) - f(x-h) /2h
module.exports = {
  fderive,
  squear,
};
