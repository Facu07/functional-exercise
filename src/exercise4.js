const wordCount = sequence => {
  if (sequence == 0 || sequence == null) {
    return null;
  } else {
    sequence = sequence.toLowerCase()
      return sequence.split(" ").reduce(function(count, word) {
        count[word] = count.hasOwnProperty(word) ? count[word] + 1 : 1;
        return count;
      }, {});
  }
};

module.exports = {
  wordCount,
};
